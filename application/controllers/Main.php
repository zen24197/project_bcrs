<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->load->library('input');
		$this->load->model('m_authModel');
	}
	public function index()
	{
		$this->load->view('customer/templates/v_header');
		$this->load->view('customer/v_home');
		$this->load->view('customer/templates/v_footer');
	}

	public function booking()
	{

		$data['court_data'] = $this->m_authModel->getCourtBookingData();

		$this->load->view('customer/templates/v_header');

		$this->load->view('customer/v_booking', $data);

		$this->load->view('customer/templates/v_footer');
	}
	public function FindCourt()
	{
		// Load the session library
		$this->load->library('session');

		// Retrieve POST data
		$data['id_customer'] = $this->input->post('user_id');
		if (empty($data['id_customer'])) {
			echo '<script>alert("Please login first.");</script>';
			echo '<script>window.location.href = "' . site_url('Main/booking') . '";</script>';
			exit();
		}
		$data_book['booking_date'] = $this->input->post('bookDate');
		$data_book['time_start'] = $this->input->post('time_start');
		$data_book['time_end'] = $this->input->post('time_end');
		$court_id = $this->input->post('court_id');



		$data_book['court_id'] = $court_id;
		$start_timestamp = strtotime($data_book['time_start']);
		$end_timestamp = strtotime($data_book['time_end']);

		if ($start_timestamp !== false && $end_timestamp !== false) {
			$duration_seconds = $end_timestamp - $start_timestamp;
			$duration_hours = $duration_seconds / 3600;

			// Send court_id and duration to getCourtPriceById
			$court_price_info = $this->m_authModel->getCourtPriceById($court_id, $duration_hours);

			$data_book['duration'] = $duration_hours;
			$data_book['price'] = $court_price_info['price'];
			$data_book['total_price'] = $court_price_info['total_price'];
		} else {
			echo "Invalid time format.";
		}

		/* 			echo '<pre>';
			print_r($data_book);
			echo '</pre>';
			die(); */
		// Store data in session variables
		$this->session->set_userdata('booking_date', $data_book['booking_date']);
		$this->session->set_userdata('time_start', $data_book['time_start']);
		$this->session->set_userdata('time_end', $data_book['time_end']);
		$this->session->set_userdata('duration', $data_book['duration']);
		$this->session->set_userdata('court_id', $data_book['court_id']);
		$this->session->set_userdata('price', $data_book['price']);
		$this->session->set_userdata('total_price', $data_book['total_price']);

		// Load the appropriate view
		$this->load->view('customer/templates/v_header');
		$this->load->view('customer/v_bookForm');  // Removed extra comma here
		$this->load->view('customer/templates/v_footer');

		/* 	// Redirect to another page
		redirect(site_url('Main/CourtDate')); */
	}

	public function CourtDate()
	{
		$this->load->view('customer/templates/v_header');
		$this->load->view('customer/v_timeList');
		$this->load->view('customer/templates/v_footer');
	}
	public function BookFinish()
	{
		$data_book['id_customer'] = $this->input->post('id_customer');
		$data_book['id_court'] = $this->input->post('id_court');
		$data_book['booking_date'] = $this->input->post('bookDate');
		$data_book['booking_start_time'] = $this->input->post('time_start');
		$data_book['booking_end_time'] = $this->input->post('time_end');
		$data_book['duration'] = $this->input->post('duration');
		$data_book['total_payment'] = $this->input->post('total_price');
		$data_book['payment_time'] = date('Y-m-d H:i:s');
		$data_book['status'] = 0;

		$bookingSuccess = $this->m_authModel->finishBook($data_book);

		if ($bookingSuccess) {

			$this->db->insert('booking', $data_book);
			echo '<script>alert("Booking Complete");</script>';
			echo '<script>window.location.href = "' . site_url('Main/booking') . '";</script>';
			exit();
		} else {
		
			echo '<script>alert("Error: Overlapping booking detected. Please choose a different time slot.");</script>';
			echo '<script>window.location.href = "' . site_url('Main/booking') . '";</script>';
			exit();
		}
	}

	/* 	public function FindCourt()
	{
		$data['id_customer'] = $this->input->post('user_id');

		if (empty($data['id_customer'])) {
			echo '<script>alert("Please login first.");</script>';
			echo '<script>window.location.href = "' . site_url('Main/booking') . '";</script>';
			exit();
		}

		$data_book['booking_date'] = $this->input->post('bookDate');
		$data_book['time_start'] = $this->input->post('time_start');
		$data_book['time_end'] = $this->input->post('time_end');

		$start_timestamp = strtotime($data_book['time_start']);
		$end_timestamp = strtotime($data_book['time_end']);

		if ($start_timestamp !== false && $end_timestamp !== false) {
			$duration_seconds = $end_timestamp - $start_timestamp;

			$duration_hours = $duration_seconds / 3600; 

			$data_book['duration'] = $duration_hours;

			echo '<pre>';
			print_r($data_book);
			echo '</pre>';
		} else {
			echo "Invalid time format.";
		}

		$this->m_authModel->findCourtData($data_book);
		$this->load->view('customer/templates/v_header');
		$this->load->view('customer/v_findCourt');
		$this->load->view('customer/templates/v_footer');
	} */
}
