<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}
	public function index()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		if ($this->form_validation->run() == false) {
			$this->load->view('auth/v_login');
		} else {
			$this->_login();
		}
	}
	public function login_admin()
	{
		$this->load->view('auth/v_login_Admin');
	}
	public function action_login_admin()
	{

		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->load->view('auth/v_login_Admin');
		} else {
			$this->_login_admin();
		}
	}

	/* 	private function _login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$user = $this->db->get_where('customer', ['email' => $email])->row_array();

		if ($user) {
			if (password_verify($password, $user['password'])) {
				$data = [
					'email' => $user['email'],
					/* 'name' => $user['name'] 
				];
				$this->session->set_userdata($data);
				redirect('main');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">wrong password</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">email is not registered</div>');
			redirect('auth');
		}
	} */

	/* 	private function _login()
{
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $customer = $this->db->get_where('customer', ['email' => $email])->row_array();
    $admin = $this->db->get_where('admin', ['email' => $email])->row_array();

    if ($customer) {

        if (password_verify($password, $customer['password'])) {
            $data = [
                'email' => $customer['email'],
            ];
            $this->session->set_userdata($data);
            redirect('Main');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Wrong password</div>');
        }
    } elseif ($admin) {

        if (password_verify($password, $admin['password'])) {
            $data = [
                'email' => $admin['email'],
            ];
            $this->session->set_userdata($data);
            redirect('Admin');  
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Wrong password</div>');
        }
    } else {
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is not registered</div>');
        redirect('auth');
    }
} */
	private function _login_admin()
	{
		$this->load->library('session');

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$login_data = $this->db->get_where('admin', ['username' => $username, 'is_active' => 1])->row_array();

		if ($login_data) {
			if (password_verify($password, $login_data['password'])) {

				$data_admin = [
					'id_admin' => $login_data['id_admin'],
					'username' => $login_data['username'],
					'is_super' => $login_data['is_super'],
				];

				$this->session->set_userdata($data_admin);
				redirect('Admin');
			} else {
				$this->session->set_flashdata('message', '<small class="text-danger ms-1">Wrong Username or Password</small>');
				redirect('auth/login_admin');
			}
		} else {
			$this->session->set_flashdata('message', '<small class="text-danger ms-1">Wrong Username or Password</small>');
			redirect('auth/login_admin');
		}
	}

	private function _login()
	{
		$this->load->library('session');

		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$login_data = $this->db->get_where('customer', ['email' => $email])->row_array();

		if ($login_data) {
			if (password_verify($password, $login_data['password'])) {
				/* 			$this->db->where('email', $email);
				$this->db->update('customer', ['is_login' => true]); */

				$data = [
					'id_customer' => $login_data['id_customer'],
					'email' => $login_data['email'],
					'name' => $login_data['name']
				];
				$this->session->set_userdata($data);
				redirect('Main');
			} else {
				$this->session->set_flashdata('message', '<small class="text-danger ms-1">Wrong Email or Password</small>');
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata('message', '<small class="text-danger ms-1">Wrong Email or Password</small>');
			redirect('auth');
		}
	}
	public function logout()
	{
		/*     $email = $this->session->userdata('email');

    $this->db->where('email', $email);
    $this->db->update('customer', ['is_login' => false]); */

		$this->session->sess_destroy();
		redirect('Main');
	}


	public function register()
	{
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('gender', 'Gender', 'required|trim');
		$this->form_validation->set_rules('nophone', 'Phone Number', 'required|trim');
		$this->form_validation->set_rules('dateOfBirth', 'Date of Birth', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[customer.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|matches[confirmPassword]', [
			'matches' => 'Passwords do not match',
			'min_length' => 'Password is too short'
		]);
		$this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'required|trim|min_length[6]|matches[password]');


		if ($this->form_validation->run() == false) {
			$this->load->view('auth/v_register');
		} else {
			$data = [
				'name' => htmlspecialchars($this->input->post('name', true)),
				'nophone' => $this->input->post('nophone', true),
				'date_of_birth' => $this->input->post('dateOfBirth'),
				'gender' => $this->input->post('gender'),
				'email' => htmlspecialchars($this->input->post('email', true)),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
			];
			$this->db->insert('customer', $data);
			redirect('auth');
		}
	}

	public function registration_form()
	{
		$this->auth_model->register_user();
	}

	public function login_form()
	{
		$this->auth_model->login_user();
	}

	public function main()
	{
		$this->load->view('Auth/index');
	}
}
