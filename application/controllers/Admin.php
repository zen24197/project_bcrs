<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//$this->load->library('input');
		$this->load->model('m_authModel');
	}


	public function index()
	{
		$this->load->library('session');
		if ($this->session->userdata('id_admin')) {
			$this->load->view('admin/templates/v_header');


			$this->load->view('admin/v_dashboard');
			$this->load->view('admin/templates/v_footer');
		} else {
			redirect('Main');
		}
	}
	public function login()
	{
		$this->load->view('login/v_login');
	}
	public function court()
	{
		$this->load->library('pagination');
		$this->load->view('admin/templates/v_header');
		$data['court'] = $this->m_authModel->getCourt();
		$this->load->view('admin/v_court', $data);
		$this->load->view('admin/modal/v_modalAddCourt');
		$this->load->view('admin/modal/v_modalEditCourt');
		$this->load->view('admin/templates/v_footer');
	}

	/*  */

	/*  */


	public function booking()
	{
		$this->load->view('admin/templates/v_header');
		$bookingData = $this->m_authModel->getCourtBookingData();
        $data['bookingData'] = $bookingData;

		$this->load->view('admin/v_booking');

		$this->load->view('admin/templates/v_footer');
	}
	public function Add_Form()
	{
		$this->load->view('admin/templates/v_header');
		$this->load->view('admin/v_add_form');
		$this->load->view('admin/templates/v_footer');
	}
	public function setUp()
	{
		// Load necessary libraries/helpers
		$this->load->helper('date');

		// Get the current date and time
		$current_date = new DateTime();
		$current_date->setTime(9, 0, 0);

		// Initialize an array to store the table data
		$table_data = array();

		// Generate table data for a month
		for ($i = 1; $i <= 31; $i++) {
			// Add an hour to the current time
			$current_date->modify('+1 hour');

			// Add date and time to the table data
			$table_data[] = array(
				'check_date' => $current_date->format('Y-m-d'),
				'check_start_time' => $current_date->format('h:i A')  // Format in 12-hour with AM/PM
			);

			// Add 1 day for the next iteration
			$current_date->modify('+1 day');
		}

		$data['table_data'] = $table_data;
		$this->load->view('admin/templates/v_header');
		$this->load->view('v_dateList', $data);
		$this->load->view('admin/templates/v_footer');
	}
    public function SaveDateTime() {
        $selected_data = $this->input->post('selected_data');

        // Assuming you have a model called "DateTime_model" to handle database operations
        $this->m_authModel->saveDateTime($selected_data);

        echo 'Data stored successfully!';
    }
	public function balance()
	{
		$this->load->view('admin/templates/v_header');

		$data['booking'] = $this->m_authModel->getBooking();
		/* $this->load->view('admin/v_court', $data); */
		/* 		$this->load->view('admin/modal/v_modalAdd');
		$this->load->view('admin/modal/v_modalEditCourt'); */


		$this->load->view('admin/v_balance', $data);
		$this->load->view('admin/templates/v_footer');
	}
	public function staff()
	{
		$this->load->view('admin/templates/v_header');

		$data['admin'] = $this->m_authModel->getStaff();

		$this->load->view('admin/v_staff', $data);
		$this->load->view('admin/templates/v_footer');
	}
	public function  Add_StaffForm()
	{
		$this->load->view('admin/templates/v_header');
		$this->load->view('admin/v_add_staff_form');
		$this->load->view('admin/templates/v_footer');
	}
	public function AddForm()
	{
		$this->load->view('admin/templates/v_header');
		$this->load->view('admin/v_add_form');
		$this->load->view('admin/templates/v_footer');
	}
	/* 	public function EditStaffForm()
	{
		$this->load->view('admin/templates/v_header');
		$this->load->view('admin/v_edit_staff_form');
		$this->load->view('admin/templates/v_footer');
	} */

	public function AddCourt()
	{
		$id_admin = $this->input->post('id_admin');
		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$condition = $this->input->post('condition');
		$price = $this->input->post('price');
		$deposit_rate = $this->input->post('deposit_rate');

		$ArrInsert = array(
			'id_admin' => $id_admin, //tempory
			'name' => $name,
			'type' => $type,
			'condition' => $condition,
			'price' => $price,
			'deposit_rate' => $deposit_rate,
		);
		/* 		echo '<pre>';
        print_r($ArrInsert);
        
        echo '</pre>';
        die(); */

		$this->m_authModel->insertCourt($ArrInsert);
		//log_message('debug', 'Add method called. Nama: ' . $nama . ', MyKad: ' . $mykad);
		redirect(site_url('Admin/Court'));
	}
	public function AddStaff()
	{
		$name = $this->input->post('name');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$hashed_password = password_hash($password, PASSWORD_BCRYPT);


		$ArrInsert = array(
			'name' => $name, //tempory
			'username' => $username,
			'password' => $hashed_password,
		);
		/*   		echo '<pre>';
        print_r($ArrInsert);
        
        echo '</pre>';
        die();   */

		$this->m_authModel->insertStaff($ArrInsert);
		//log_message('debug', 'Add method called. Nama: ' . $nama . ', MyKad: ' . $mykad);
		redirect(site_url('Admin/Staff'));
	}
	public function Edit_Staff($data)
	{
		$this->load->view('admin/templates/v_header');
		$query = $this->m_authModel->getEditStaffData($data);

		$info = array('row_edit' => $query);
		/* 		echo '<pre>';
		print_r($info);
		echo '</pre>';
		die(); */
		$this->load->view('admin/v_edit_staff_form', $info);
		$this->load->view('admin/templates/v_footer');
	}
	public function Edit_StaffForm()
	{

		$id = $this->input->post('id_admin');
		$ArrUpdate['username'] = $this->input->post('username');
		$ArrUpdate['is_active'] = $this->input->post('is_active');

		$checkPassword = $this->input->post('checkPassword');
		if ($checkPassword === '1') { // The checkbox is checked
			$password = $this->input->post('password');
			$ArrUpdate['password'] = password_hash($password, PASSWORD_BCRYPT);
			/* $ArrUpdate['password'] = $this->input->post('password'); */
		}



		/* 	 	echo '<pre>';
		print_r($id);
		print_r($checkPassword);
		print_r($ArrUpdate);

		echo '</pre>';die();  */

		$this->m_authModel->updateStaff($id, $ArrUpdate);
		redirect(site_url('Admin/staff'));
	}

	public function DeleteCourt($data)
	{
		$this->m_authModel->deleteCourt($data);
		redirect(site_url('Admin/Court'));
	}
	public function EditCourt()
	{
		$id = $this->input->post('idCourt');
		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$condition = $this->input->post('condition');
		$price = $this->input->post('price');
		$deposit_rate = $this->input->post('deposit_rate');

		$ArrUpdate = array(
			'name' => $name,
			'type' => $type,
			'condition' => $condition,
			'price' => $price,
			'deposit_rate' => $deposit_rate,
		);
		/* 		echo '<pre>';
		print_r($id);
        print_r($ArrUpdate);
        
        echo '</pre>';
        die(); */
		$this->m_authModel->updateCourt($id, $ArrUpdate);
		Redirect(site_url('Admin/Court'));
	}
	public function EditStatus()
	{
		$id = $this->input->post('idCourt');

		$status = $this->input->post('status');

		$ArrUpdate = array(

			'status' => $status,
		);
		$this->m_authModel->updateStatus($id, $ArrUpdate);
		Redirect(site_url('Admin/balance'));
	}
	public function Edit_Form($data)
	{
		$this->load->view('admin/templates/v_header');
		$query = $this->m_authModel->getEditData($data);
		$info = array('row_edit' => $query);
		$this->load->view('admin/v_edit_form', $info);
		$this->load->view('admin/templates/v_footer');
	}
	public function EditStatus_Form($data)
	{
		$this->load->view('admin/templates/v_header');
		$query = $this->m_authModel->getEditStatusData($data);
		$info = array('row_edit' => $query);
		$this->load->view('admin/v_editstatus_form', $info);
		$this->load->view('admin/templates/v_footer');
	}
	public function detailstatus_Form($data)
	{
		$this->load->view('admin/templates/v_header');
		$query = $this->m_authModel->getDetailStatusData($data);
		$info = array('row_detail' => $query);
		$this->load->view('admin/v_detailstatus_form', $info);
		$this->load->view('admin/templates/v_footer');
	}
}
