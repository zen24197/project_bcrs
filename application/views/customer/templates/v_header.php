<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://kit.fontawesome.com/c52decb24a.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/styles.css" />

	<!-- Datatables CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/datatables/dataTables.bootstrap5.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/datatables/buttons.bootstrap5.min.css">
	<!-- End Datatables CSS -->

	<!-- Select2 CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/select2/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/select2/select2-bootstrap-5-theme.min.css" />
	<!-- End Select2 CSS -->

	<!-- SweetAlert2 -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.css" />
	<!-- End SweetAlert2 -->

	<!-- Paypal -->
	<script src="https://www.paypal.com/sdk/js?client-id=AXbatEv_y7xmF5dTn6ZIi7EYcIz2bqHbAPkJljuOZ-wsQTengoxpYH7UcBjtl4ybIcORYPRt5mbStLro&components=ECsB0799FSntUDpin7IltGo6UzI764D4cN5dRCFmxgv8RDTzzUJiowzErxw5bF6vlZ6gDktCgheGGshN"></script>


	<script src="<?php echo base_url(); ?>js/jquery-3.6.4.js"></script>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<title>Document</title>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand ms-2" href="<?php echo site_url(); ?>Main">BCRS</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse justify-content-between" id="navbarColor01">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="<?php echo site_url(); ?>Main">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url(); ?>Main/booking">Booking</a>
				</li>

			</ul>
			<?php
			$user_name = $this->session->userdata('name');
			$id_customer = $this->session->userdata('id_customer');
			?>
			<ul class="navbar-nav">
				<li class="nav-item dropdown">
					<?php if ($user_name) { ?>
						<a class="nav-link dropdown-toggle me-5 text-light" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo $user_name; ?>
						</a>
						<div class="dropdown-menu" aria-labelledby="userDropdown">
							<!-- <a class="dropdown-item" href="#">Profile</a> -->
							<a class="dropdown-item" href="<?php echo site_url(); ?>Auth/logout">Logout</a>
						</div>
					<?php } else { ?>
						<a class="text-light me-5" href="<?php echo site_url(); ?>Auth/index">Login</a>
					<?php } ?>
				</li>
			</ul>

			</li>



			<!-- Customer Section moved to the right -->
			<!--             <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fas fa-user fa-fw"></i> Customer
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="<?php echo site_url(); ?>Auth/index">Login</a></li>
                        <li>
                            <hr class="dropdown-divider" />
                        </li>
                        <li><a class="dropdown-item" href="<?php echo site_url(); ?>Auth/register">Register</a></li>
                    </ul>
                </li>
            </ul> -->
		</div>
	</nav>
</body>

</html>
