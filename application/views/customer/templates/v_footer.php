<div id="layoutAuthentication_footer">
	<footer class="py-4 bg-light mt-auto">
		<div class="container-fluid px-4">
			<div class="d-flex align-items-center justify-content-between small">
				<div class="text-muted">Copyright &copy; Your Website 2023</div>
				<div>
					<a href="#">Privacy Policy</a>
					&middot;
					<a href="#">Terms &amp; Conditions</a>
				</div>
			</div>
		</div>
	</footer>
</div>
</div>
<script src="<?php echo base_url(); ?>js/bootstrap@5.2.3/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script src="<?php echo base_url(); ?>js/fontawesome-6.3.0/all.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap@5.2.3/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script src="<?php echo base_url(); ?>js/my-scripts.js"></script>

<!-- Datatables JS -->
<script src="<?php echo base_url(); ?>js/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/dataTables.bootstrap5.min.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/buttons.bootstrap5.min.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/ajax/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/ajax/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/ajax/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/buttons/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/buttons/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>js/datatables/my-script-dataTables.js"></script>
<!-- End Datatables JS -->

<!-- Select2 JS -->
<script src="<?php echo base_url(); ?>js/select2/select2.min.js"></script>
<!-- End Select2 JS -->

<!-- Sweet Alerts js -->
<script src="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.js"></script>
<!-- Sweet Alerts js ends -->

<script src="<?php echo base_url(); ?>js/chartjs/chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/demo/chart-area-demo.js"></script>
<script src="<?php echo base_url(); ?>assets/demo/chart-bar-demo.js"></script>



</html>
</body>

</html>
