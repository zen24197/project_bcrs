<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid px-4 mt-4">
			<div id="carouselExample" class="carousel slide" data-bs-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img src="<?= base_url('assets/img/3.png'); ?>" class="d-block w-100" alt="Slide 1" style="width: 1350px !important; height: 400px !important;">
					</div>
					<div class="carousel-item">
						<img src="<?= base_url('assets/img/1.png'); ?>" class="d-block w-100" alt="Slide 2" style="width: 500px; height: 400px;">
					</div>
					<div class="carousel-item">
						<img src="<?= base_url('assets/img/2.png'); ?>" class="d-block w-100" alt="Slide 3" style="width: 500px; height: 400px;">
					</div>
				</div>
				<button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Previous</span>
				</button>
				<button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Next</span>
				</button>
			</div>

		</div>
	</main>
</div>

</div>
