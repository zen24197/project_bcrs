<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid px-4 mt-4 ">
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item">Booking</li>
				<li class="breadcrumb-item active">Confirmation</li>
			</ol>
			<div class="row">
				<div class="col-md-6 offset-md-1">

					<form action="<?php echo site_url("Main/BookFinish") ?>" method="post">

						<input type="hidden" class="form-control" id="court_id" name="id_court" readonly value="<?php echo $this->session->userdata('court_id'); ?>">
						<input type="hidden" class="form-control" id="id_customer" name="id_customer" readonly value="<?php echo $this->session->userdata('id_customer');; ?>">

						<div class="form-group">
							<label for="courtName">Booking Date</label>
							<input type="date" class="form-control" id="bookDate" name="bookDate" readonly value="<?php echo $this->session->userdata('booking_date'); ?>">
						</div>
						<div class="form-group mt-3">
							<label for="courtName">Time Start:</label>
							<input type="time" class="form-control" id="time_start" name="time_start" readonly value="<?php echo $this->session->userdata('time_start'); ?>">
						</div>
						<div class="form-group mt-3">
							<label for="courtName">Time End:</label>
							<input type="time" class="form-control" id="time_end" name="time_end" readonly value="<?php echo $this->session->userdata('time_end'); ?>">
						</div>
						<div class="form-group mt-3">
							<label for="duration">Duration:</label>
							<input type="number" class="form-control" id="duration" name="duration" readonly value="<?php echo $this->session->userdata('duration'); ?>">
						</div>
						<div class="form-group mt-3">
							<label for="price">Court Price:</label>
							<input type="number" class="form-control" id="price" name="price" readonly value="<?php echo $this->session->userdata('price'); ?>">
						</div>
						<div class="form-group mt-3">
							<label for="total_price">Court Price:</label>
							<input type="number" class="form-control" id="total_price" name="total_price" readonly value="<?php echo $this->session->userdata('total_price'); ?>">
						</div>
						<!-- <input type="hidden" class="form-control" id="time_end" name="time_end" readonly value="<?php echo $this->session->userdata('duration'); ?>"> -->
						<div class="btn-group mt-3" role="group" aria-label="Booking Actions">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo site_url('Main/booking') ?>" class="btn btn-danger ms-3 ml-2">Back</a>
						</div>

					</form>
				</div>
			</div>
		</div>
	</main>


</div>
</div>
