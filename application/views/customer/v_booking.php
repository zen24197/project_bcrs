<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid px-4 mt-4 ">
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item">Booking</li>
			</ol>
			<div class="row">
				<div class="col-md-6 offset-md-1">
					<form method="POST" action="<?php echo site_url("Main/FindCourt") ?>">
						<input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $id = $this->session->userdata('id_customer'); ?>" required>
						<div class="mb-3 form-check">
							<label for="bookDate" class="form-label">Booking Date? </label>
							<input type="date" class="form-control" id="bookDate" name="bookDate" required>
						</div>
						<div class="mb-3 form-check">
							<label for="time1" class="form-label">Booking Time?</label>
							<select class="form-control" id="time1" name="time_start" required>
								<option value="09:00">09:00 AM</option>
								<option value="10:00">10:00 AM</option>
								<option value="11:00">11:00 AM</option>
								<option value="12:00">12:00 PM</option>
								<option value="13:00">01:00 PM</option>
								<option value="14:00">02:00 PM</option>
								<option value="15:00">03:00 PM</option>
								<option value="16:00">04:00 PM</option>
							</select>
						</div>
						<div class="mb-3 form-check">
							<label for="time2" class="form-label">Booking Time?</label>
							<select class="form-control" id="time2" name="time_end" required>
								<option value="10:00">10:00 AM</option>
								<option value="11:00">11:00 AM</option>
								<option value="12:00">12:00 PM</option>
								<option value="13:00">01:00 PM</option>
								<option value="14:00">02:00 PM</option>
								<option value="15:00">03:00 PM</option>
								<option value="16:00">04:00 PM</option>
								<option value="17:00">05:00 AM</option>
							</select>
						</div>
						<div class="mb-3 form-check">
							<label for="court_id" class="form-label">Court ?</label>
							<select class="form-control" id="court_id" name="court_id" required>
								<?php foreach ($court_data as $court) : ?>
									<option value="<?php echo $court->id_court; ?>"><?php echo $court->name; ?> | <?php echo $court->type; ?></option>
								<?php endforeach; ?>
							</select>
						</div>


						<!-- 	<div class="mb-3 form-check">
				<label for="hour" class="form-label">How Many Hour?</label>
				<input type="number" class="form-control" id="hour" name="hour" min="0" max="6" required value="1" oninput="handleInputChange()">
			</div> -->
						<!-- 
			<div id="end-time"></div>
			<a href="<?php echo site_url('Admin/Edit_Form/') ?>/<?php echo $row->id_court; ?>" class="btn btn-warning"><i class="fa-solid fa-pen-to-square" style="color: #ffffff;"></i></a>  
 -->
						<center><button type="submit" class="btn btn-primary">Next</button></center>
					</form><!--  -->
				</div>
			</div>
		</div>
	</main>


</div>
</div>

<script>
	// Get the current date in the format YYYY-MM-DD
	var currentDate = new Date().toISOString().slice(0, 10);

	var maxDate = new Date();
	maxDate.setMonth(maxDate.getMonth() + 3);
	var formattedMaxDate = maxDate.toISOString().slice(0, 10);

	document.getElementById("bookDate").value = currentDate;
	document.getElementById("bookDate").min = currentDate;
	document.getElementById("bookDate").max = formattedMaxDate;


	/* function handleInputChange() {
	// Get the input elements
	var inputTime = document.getElementById("time").value;
	var inputDuration = parseInt(document.getElementById("hour").value);

	// Parse inputTime as hours and minutes
	var startTime = inputTime.split(":");
	var startHours = parseInt(startTime[0]);
	var startMinutes = parseInt(startTime[1]);

	// Calculate end time
	var endHours = (startHours + inputDuration) % 24;
	var endMinutes = startMinutes;

	// Format end time as HH:MM
	var formattedEndHours = (endHours < 10 ? '0' : '' ) + endHours; var formattedEndMinutes=(endMinutes < 10 ? '0' : '' ) + endMinutes; var endTime=formattedEndHours + ":" + formattedEndMinutes; // Display end time document.getElementById("end-time").textContent="End Time: " + endTime; } // Get the current date in the format YYYY-MM-DD var currentDate=new Date().toISOString().slice(0, 10); // Set the default value and minimum date of the input field to the current date document.getElementById("bookDate").value=currentDate; document.getElementById("bookDate").min=currentDate; */
</script>

</script>
