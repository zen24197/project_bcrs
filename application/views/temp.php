<!-- Example Bootstrap Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Modal content goes here -->
            <div class="modal-body">
                <!-- Display the court_id from the session -->
                Court ID: <?php echo $this->session->userdata('court_id'); ?>
            </div>
        </div>
    </div>
</div>




public function set_court_id_session() {
    $court_id = $this->input->post('court_id');
    $this->session->set_userdata('court_id', $court_id);

    // You can optionally return a response if needed
    // Example: echo json_encode(['message' => 'Court ID set successfully']);
}


<script>
function sendCourtIdToSession(courtId) {
    // Use AJAX to send the court_id to a CodeIgniter controller method
    $.ajax({
        url: "<?php echo site_url('your_controller/set_court_id_session'); ?>",
        type: "POST",
        data: { court_id: courtId },
        success: function(response) {
            // Handle success (if needed)
            // You can display a modal here after successful completion
            // Example: $('#myModal').modal('show');
        },
        error: function() {
            // Handle errors (if needed)
        }
    });
}
</script>

<button class="btn btn-warning" onclick="sendCourtIdToSession(<?php echo $row->id_court; ?>)">
    <i class="fa-solid fa-pen-to-square" style="color: #ffffff;"></i>
</button>
