<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4 mt-4">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Balance</li>
            </ol>
            <h1>Table of Dates and Times</h1>
            <button id="checkAllBtn" class="btn btn-primary mb-2">Check All</button>
            <table id="CTable" class="table table-bordered mb-4">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Select</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($table_data as $row) : ?>
                        <tr>
                            <td><?php echo $row['check_date']; ?></td>
                            <td><?php echo $row['check_start_time']; ?></td>
                            <td><input type="checkbox" name="selected_dates[]" value="<?php echo $row['check_date']; ?>"></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <button id="storeDataBtn" class="btn btn-success mb-2">Store Checked Data</button>
        </div>
    </main>
</div>

<script>
    document.getElementById('storeDataBtn').addEventListener('click', function () {
        var checkboxes = document.querySelectorAll('input[type="checkbox"][name="selected_dates[]"]:checked');
        var selectedData = [];

        checkboxes.forEach(function (checkbox) {
            var rowData = {
                check_date: checkbox.parentElement.parentElement.querySelector('td:nth-child(1)').innerText,
                check_start_time: checkbox.parentElement.parentElement.querySelector('td:nth-child(2)').innerText
            };
            selectedData.push(rowData);
        });

        if (selectedData.length > 0) {
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('Admin/SaveDateTime'); ?>',
                data: { selected_data: selectedData },
                success: function (response) {
                    console.log('Data stored successfully:', response);
                },
                error: function (error) {
                    console.error('Error storing data:', error);
                }
            });
        } else {
            alert('No dates selected.');
        }
    });

    document.getElementById('checkAllBtn').addEventListener('click', function () {
        var checkboxes = document.querySelectorAll('input[type="checkbox"][name="selected_dates[]"]');
        checkboxes.forEach(function (checkbox) {
            checkbox.checked = true;
        });
    });
</script>

