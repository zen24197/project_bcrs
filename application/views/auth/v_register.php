<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<title>Login - SB Admin</title>
	<link href="<?php echo site_url(); ?>css/styles.css" rel="stylesheet" />
	<script src="<?php echo site_url(); ?>js/fontawesome-6.3.0/all.js"></script>
	<script src="<?php echo site_url(); ?>js/jquery-3.6.4.js"></script>
</head>

<body class="bg-dark">
	<div id="layoutAuthentication">
		<div id="layoutAuthentication_content">
			<main>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-7">
							<div class="card shadow-lg border-0 rounded-lg mt-5">
								<div class="card-header">
									<h3 class="text-center font-weight-light my-4">Create Account</h3>
								</div>
								<div class="card-body">
									<form method="post" action="<?php base_url('auth/register') ?>">
										<?php /* echo form_open('Auth/registration_form') */ ?>
										<div class="row mb-3">
											<div class="col-md-6">
												<div class="form-floating mb-3 mb-md-0">
													<input class="form-control" id="inputFirstName" type="text" placeholder="Enter your full Name" name="name" value="<?= set_value('name') ?>" />
													<label for="inputFirstName">Full Name</label>
													<?= form_error('name', '<small class="text-danger ms-1">', '</small>') ?>
												</div>
											</div>
										</div>
										<div class="row mb-3">
											<div class="col-md-6">
												<label for="inputGender">Gender</label>
												<div class="form-check">
													<input class="form-check-input" type="radio" name="gender" id="maleGender" value="Male">
													<label class="form-check-label" for="maleGender">
														Male
													</label>
												</div>
												<div class="form-check">
													<input class="form-check-input" type="radio" name="gender" id="femaleGender" value="Female">
													<label class="form-check-label" for="femaleGender">
														Female
													</label>
												</div>
											</div>
										</div>
										<!-- Phone -->
										<div class="form-floating mb-3">
                                                <input class="form-control" id="inputPhone" type="tel" placeholder="Enter your phone number" name="nophone" value="<?= set_value('nophone') ?>"/>
                                                <label for="inputPhone">Phone</label>
                                                <?= form_error('nophone','<small class="text-danger ms-1">','</small>') ?>
                                            </div>
                                            
                                            <!-- Date of Birth -->
                                            <div class="form-floating mb-3">
                                                <input class="form-control" id="inputDateOfBirth" type="date" name="dateOfBirth" />
                                                <label for="inputDateOfBirth">Date of Birth</label>
                                                <?= form_error('dateOfBirth','<small class="text-danger ms-1">','</small>') ?>
                                            </div>
										<div class="form-floating mb-3">
											<input class="form-control" id="inputEmail" type="email" placeholder="name@example.com" name="email" value="<?= set_value('email') ?>" />
											<label for="inputEmail">Email address</label>
											<?= form_error('email', '<small class="text-danger ms-1">', '</small>') ?>
										</div>
										<div class="row mb-3">
											<div class="col-md-6">
												<div class="form-floating mb-3 mb-md-0">
													<input class="form-control" id="inputPassword" type="password" placeholder="Create a password" name="password" value="<?= set_value('password') ?>" />
													<label for="inputPassword">Password</label>
													<?= form_error('password', '<small class="text-danger ms-1">', '</small>') ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-floating mb-3 mb-md-0">
													<input class="form-control" id="inputPasswordConfirm" type="password" placeholder="Confirm password" name="confirmPassword" />
													<label for="inputPasswordConfirm">Confirm Password</label>
													<?= form_error('confirmPassword', '<small class="text-danger ms-1">', '</small>') ?>
												</div>
											</div>
										</div>
										<div class="mt-4 mb-0">

											<div class="d-grid"><button type="submit" class="btn btn-primary btn-block">Create Account</button></div>
										</div>
									</form>
									<?php /* echo form_close() */ ?>
								</div>
								<div class="card-footer text-center py-3">
									<div class="small"><a href="<?php echo site_url(); ?>Auth/index">Have an account? Go to login</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

