<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<title>Login - SB Admin</title>
	<link href="<?php echo site_url(); ?>css/styles.css" rel="stylesheet" />
	<script src="<?php echo site_url(); ?>js/fontawesome-6.3.0/all.js"></script>
	<script src="<?php echo site_url(); ?>js/jquery-3.6.4.js"></script>
</head>

<body class="bg-dark">
	<div id="layoutAuthentication">
		<div id="layoutAuthentication_content">

			<main>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-5">
							<div class="card shadow-lg border-0 rounded-lg mt-5">
								<div class="card-header">
									<h3 class="text-center font-weight-light my-4">Login <i class="fa-solid fa-user"></i></h3>
								</div>
								<div class="card-body">
									<form method="post" action="<?php site_url('auth/login_customer') ?>">
										<div class="form-floating mb-3">
											<input class="form-control" id="inputEmail" type="email" placeholder="name@example.com" name="email" value="<?= set_value('email') ?>" />
											<label for="inputEmail">Email address</label>
											<?= form_error('email', '<small class="text-danger ms-1">', '</small>') ?>
											<?php echo $this->session->flashdata('message'); ?>
										</div>
										<div class="form-floating mb-3">
											<input class="form-control" id="inputPassword" type="password" placeholder="Password" name="password" />
											<label for="inputPassword">Password</label>
											<?= form_error('password', '<small class="text-danger ms-1">', '</small>') ?>
										</div>
	
										<div class="d-grid gap-2">
                                            <button type="submit" class="btn btn-primary btn-lg mt-1">Login</button>
											<hr class="my-1">
                                            <a class="btn btn-secondary btn-lg mt-1" href="<?php echo site_url(); ?>">Home</a>
                                            <a class="btn btn-primary btn-lg mt-1" href="<?php echo site_url(); ?>auth/login_admin">Login Admin</a>
                                        </div>
									</form>
								</div>
								<div class="card-footer text-center py-3">
									<div class="small"><a href="<?php echo site_url(); ?>Auth/register">Need an account? Sign up!</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>

		</div>

	</div>
	<script src="<?php echo site_url(); ?>js/bootstrap@5.2.3/bootstrap.bundle.min.js"></script>
	<script src="<?php echo site_url(); ?>js/scripts.js"></script>
</body>

</html>
