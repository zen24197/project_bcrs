<div id="layoutSidenav_content">
	<div class="container-fluid px-4 mt-4 ">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?= site_url('Admin/staff') ?>">Staff</a></li>
			<li class="breadcrumb-item active">Edit Staff</a></li>
		</ol>
		<div class="row">
			<div class="col-md-6 offset-md-1">
				<form method="post" action="<?php echo site_url('Admin/Edit_StaffForm') ?>">
					<input type="hidden" class="form-control" id="name" name="id_admin" value="<?Php echo $row_edit->id_admin ?>">
					<div class="form-group mt-3">
						<label for="name">Staff Username</label>
						<input type="text" class="form-control" id="name" placeholder="Enter Username" name="username" value="<?Php echo $row_edit->username ?>">
					</div>
					<div class="form-group mt-3">
						<label for="name">Change Password (required)</label><br>
						<input type="radio" id="changePasswordRadio" onclick="togglePasswordInput()" name="checkPassword" value="1"> Yes
						<input type="radio" id="changePasswordRadio" onclick="togglePasswordInput()" name="checkPassword" value="0"> No
					</div>

					<div class="form-group mt-3" id="passwordGroup" style="display: none;">
						<label for="password">Staff Password</label>
						<input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" required>
						<?= form_error('password', '<small class="text-danger ms-1">', '</small>') ?>
					</div>
					<div class="form-group mt-3">
						<label for="username">Permision</label>
						<select class="form-control" id="active" name="is_active">
							<option value="0" <?php echo ($row_edit->is_active == '0') ? 'selected' : ''; ?>>Deactive</option>
							<option value="1" <?php echo ($row_edit->is_active == '1') ? 'selected' : ''; ?>>Active</option>
						</select>
					</div>
					<div class="btn-group" role="group" aria-label="Button group">
						<button type="submit" class="btn btn-primary mt-3">Submit</button>
						<a href="<?php echo base_url('Admin/Staff') ?>" class="btn btn-danger ml-2 mt-3">Back</a>
					</div>

				</form>
			</div>
		</div>
	</div>

	<script>
		function togglePasswordInput() {
			var passwordGroup = document.getElementById('passwordGroup');
			var radioYes = document.getElementById('changePasswordRadio');
			var passwordInput = document.getElementById('password');

			if (radioYes.checked) {
				passwordGroup.style.display = 'block';
				passwordInput.required = true;
			} else {
				passwordGroup.style.display = 'none';
				passwordInput.required = false;
			}
		}
	</script>

</div>
