<div id="layoutSidenav_content">
	<div class="container-fluid px-4 mt-4 ">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?= site_url('Admin/balance') ?>">Booking</a></li>
			<li class="breadcrumb-item active">Edit Booking</a></li>
		</ol>
		<div class="row">
			<div class="col-md-6 offset-md-1">
				<form method="post" action="<?php echo site_url('Admin/EditStatus') ?>">
					<input type="hidden" class="form-control" id="stdId" name="idCourt" value="<?Php echo $row_edit->id_booking ?>" required>
					<div class="form-group mt-5">
						<label for="courtName">Customer Name</label>
						<input type="text" class="form-control" id="id_customer" placeholder="Enter Court Name" readonly name="id_customer" value="<?Php echo $row_edit->id_customer ?>">
					</div>
					<div class="form-group mt-5">
						<label for="courtName">Court Name</label>
						<input type="text" class="form-control" id="id_court" placeholder="Enter Court Name" readonly name="id_court" value="<?Php echo $row_edit->id_court ?>">
					</div>
					<div class="form-group">
						<label for="courtPrice">Booking Date </label>
						<input type="date" class="form-control" id="courtPrice" placeholder="Enter Court Price" readonly name="booking_date" value="<?Php echo $row_edit->booking_date ?>">
					</div>
					<div class="form-group">
						<label for="booking_stime">Booking Start Time</label>
						<input type="time" class="form-control" id="booking_stime" placeholder="Enter Deposit Rate" readonly name="booking_start_time" value="<?Php echo $row_edit->booking_start_time ?>">
					</div>
					<div class="form-group">
						<label for="booking_etime">Booking End Time</label>
						<input type="time" class="form-control" id="booking_etime" placeholder="Enter Deposit Rate" readonly name="booking_end_time" value="<?Php echo $row_edit->booking_end_time ?>">
					</div>
					<div class="form-group">
						<label for="duration">Duration</label>
						<input type="number" class="form-control" id="duration" placeholder="Enter Duration" readonly name="duration" value="<?Php echo $row_edit->duration ?>">
					</div>
					<div class="form-group">
						<label for="total_payment">Total Payment</label>
						<input type="number" class="form-control" id="total_payment" readonly placeholder="Enter total_payment" name="total_payment" step="0.01" value="<?Php echo $row_edit->total_payment ?>">
					</div>

					<div class="form-group">
						<label for="status">Status</label>
						<select class="form-control" id="status" name="status">
							<option value="0" <?php echo ($row_edit->status == 0) ? 'selected' : ''; ?>>Pending</option>
							<option value="1" <?php echo ($row_edit->status == 1) ? 'selected' : ''; ?>>Refund</option>
							<option value="2" <?php echo ($row_edit->status == 2) ? 'selected' : ''; ?>>Complete</option>
						</select>
					</div>

					<div>
						<button type="submit" class="btn btn-primary mt-3">Update</button>
						<a href="<?php echo base_url('Admin/balance') ?>" class="btn btn-danger ml-5 mt-3">Back</a>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
