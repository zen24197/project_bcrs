<div class="modal fade" id="myModalEdit">
				<div class="modal-dialog">
					<div class="modal-content">
						<form method="post" action="<?php echo site_url('Admin/EditCourt'); ?>">
							<!-- Modal Header -->
							<div class="modal-header">
								<h4 class="modal-title">Edit Court</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

							<!-- Modal body -->
							<div class="modal-body">
								<!-- Add an input field to hold the court ID -->
								<input type="hidden" class="form-control" id="editCourtId" name="idCourt" required>
								<div class="form-group mt-5">
									<div class="form-group">
										<label for="courtName">Court Name</label>
										<input type="text" class="form-control" id="courtName" placeholder="Enter Court Name" name="name" value="<?php  echo $row->id_court;  ?>" require>
									</div>
									<div class="form-group">
										<label for="courtType">Court Type</label>
										<select class="form-control" id="courtType" name="type">
											<option value="Premium Indoor">Premium Indoor</option>
											<option value="Normal Indoor">Normal Indoor</option>
											<option value="Outdoor">Outdoor</option>
										</select>
									</div>
									<div class="form-group">
										<label for="courtCondition">Court Condition</label>
										<select class="form-control" id="courtCondition" name="condition">
											<option value="good">Good</option>
											<option value="average">Average</option>
											<option value="poor">Poor</option>
										</select>
									</div>
									<div class="form-group">
										<label for="courtPrice">Court Price (RM) </label>
										<input type="number" class="form-control" id="courtPrice" placeholder="Enter Court Price" name="price"  step="0.01">
									</div>
									<div class="form-group">
										<label for="depositRate">Deposit Rate (%)</label>
										<input type="number" class="form-control" id="depositRate" placeholder="Enter Deposit Rate" name="deposit_rate" step="0.01">
									</div>


								</div>
								<!-- Add other form fields here for editing court data -->
							</div>
							<!-- Modal footer -->
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Update</button>
							</div>
						</form>
					</div>
				</div>
			</div>
</div>
