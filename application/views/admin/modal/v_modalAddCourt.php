	<!-- Modal for Adding or Editing Data -->
	<div class="modal fade" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<form method="post" action="<?php echo site_url('Admin/AddCourt') ?>">
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Add Court</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						<div class="form-group">
						<input type="hidden" class="form-control" id="id_admin" name="id_admin" value="<?php echo $id = $this->session->userdata('id_admin'); ?>" required>
							<label for="courtName">Court Name</label>
							<input type="text" class="form-control" id="courtName" placeholder="Enter Court Name" name="name" require>
						</div>
						<div class="form-group">
							<label for="courtType">Court Type</label>
							<select class="form-control" id="courtType" name="type">
								<option value="Premium Indoor">Premium Indoor</option>
								<option value="Normal Indoor">Normal Indoor</option>
								<option value="Outdoor">Outdoor</option>
							</select>
						</div>
						<div class="form-group">
							<label for="courtCondition">Court Condition</label>
							<select class="form-control" id="courtCondition" name="condition">
								<option value="good">Good</option>
								<option value="average">Average</option>
								<option value="poor">Poor</option>
							</select>
						</div>
						<div class="form-group">
							<label for="courtPrice">Court Price (RM)</label>
							<input type="number" class="form-control" id="courtPrice" placeholder="Enter Court Price" name="price" step="0.01">
						</div>
						<div class="form-group">
							<label for="depositRate">Deposit Rate (%)</label>
							<input type="number" class="form-control" id="depositRate" placeholder="Enter Deposit Rate" name="deposit_rate" step="0.01">
						</div>

					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>


