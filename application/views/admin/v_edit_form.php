<div id="layoutSidenav_content">
	<div class="container-fluid px-4 mt-4 ">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?= site_url('Admin/court') ?>">Court</a></li>
			<li class="breadcrumb-item active">Edit Court</a></li>
		</ol>
		<div class="row">
			<div class="col-md-6 offset-md-1">
				<form method="post" action="<?php echo site_url('Admin/EditCourt') ?>">
					<input type="hidden" class="form-control" id="idCourt" name="idCourt" value="<?Php echo $row_edit->id_court ?>" required>
					<div class="form-group mt-3">
						<label for="courtName">Court Name</label>
						<input type="text" class="form-control" id="courtName" placeholder="Enter Court Name" name="name" value="<?Php echo $row_edit->name ?>">
					</div>
					<div class="form-group">
						<label for="courtType">Court Type</label>
						<select class="form-control" id="courtType" name="type">
							<option value="Premium Indoor" <?php echo ($row_edit->type == 'Premium Indoor') ? 'selected' : ''; ?>>Premium Indoor</option>
							<option value="Normal Indoor" <?php echo ($row_edit->type == 'Normal Indoor') ? 'selected' : ''; ?>>Normal Indoor</option>
							<option value="Outdoor" <?php echo ($row_edit->type == 'Outdoor') ? 'selected' : ''; ?>>Outdoor</option>
						</select>
					</div>
					<div class="form-group">
						<label for="courtCondition">Court Condition</label>
						<select class="form-control" id="courtCondition" name="condition">
							<option value="good" <?php echo ($row_edit->condition == 'good') ? 'selected' : ''; ?>>Good</option>
							<option value="average" <?php echo ($row_edit->condition == 'average') ? 'selected' : ''; ?>>Average</option>
							<option value="poor" <?php echo ($row_edit->condition == 'poor') ? 'selected' : ''; ?>>Poor</option>
						</select>
					</div>
					<div class="form-group">
						<label for="courtPrice">Court Price (RM)</label>
						<input type="number" class="form-control" id="courtPrice" placeholder="Enter Court Price" name="price" step="0.01" value="<?Php echo $row_edit->price ?>">
					</div>
					<div class="btn-group" role="group" aria-label="Button group">
						<button type="submit" class="btn btn-primary mt-3">Update</button>
						<a href="<?php echo site_url('Admin/Court') ?>" class="btn btn-danger ml-2 mt-3">Back</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
