<div id="layoutSidenav_content">
	<div class="container-fluid px-4 mt-4 ">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?= site_url('Admin/court') ?>">Court</a></li>
			<li class="breadcrumb-item active">Add Court</a></li>
		</ol>
		<div class="row">
			<div class="col-md-6 offset-md-1">
				<form method="post" action="<?php echo site_url('Admin/AddCourt') ?>" >
				<input type="hidden" class="form-control" id="id_admin" name="id_admin" value="<?php echo $id = $this->session->userdata('id_admin'); ?>" required>
					<div class="form-group mt-3">
						<label for="courtName">Court Name</label>
						<input type="text" class="form-control" id="courtName" placeholder="Enter Court Name" name="name">
					</div>
					<div class="form-group">
						<label for="courtType">Court Type</label>
						<select class="form-control" id="courtType" name="type">
							<option value="Premium Indoor">Premium Indoor</option>
							<option value="Normal Indoor">Normal Indoor</option>
							<option value="Outdoor">Outdoor</option>
						</select>
					</div>
					<div class="form-group">
						<label for="courtCondition">Court Condition</label>
						<select class="form-control" id="courtCondition" name="condition">
							<option value="good">Good</option>
							<option value="average">Average</option>
							<option value="poor">Poor</option>
						</select>
					</div>
					<div class="form-group">
						<label for="courtPrice">Court Price (RM)</label>
						<input type="number" class="form-control" id="courtPrice" placeholder="Enter Court Price" name="price" step="0.01">
					</div>
					<div class="form-group">
						<label for="depositRate">Deposit Rate (%)</label>
						<input type="number" class="form-control" id="depositRate" placeholder="Enter Deposit Rate" name="deposit_rate" step="0.01">
					</div>
					<div class="btn-group" role="group" aria-label="Button group">
						<button type="submit" class="btn btn-primary mt-3">Submit</button>
						<a href="<?php echo base_url('Admin/Court')?>" class="btn btn-danger ml-2 mt-3">Back</a>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
