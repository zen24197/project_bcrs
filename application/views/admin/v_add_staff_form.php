<div id="layoutSidenav_content">
	<div class="container-fluid px-4 mt-4 ">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?= site_url('Admin/staff') ?>">Staff</a></li>
			<li class="breadcrumb-item active">Add Court</a></li>
		</ol>
		<div class="row">
			<div class="col-md-6 offset-md-1">
				<form method="post" action="<?php echo site_url('Admin/AddStaff') ?>">
					
					<div class="form-group mt-3">
						<label for="name">Staff Name</label>
						<input type="text" class="form-control" id="name" placeholder="Enter Staff Name" name="name">
					</div>
					<div class="form-group mt-3">
						<label for="username">Username</label>
						<input type="text" class="form-control" id="username" placeholder="Enter Username" name="username">
					</div>
					<div class="form-group mt-3">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" placeholder="Enter Password" name="password">
					</div>

					<div class="btn-group" role="group" aria-label="Button group">
						<button type="submit" class="btn btn-primary mt-3">Submit</button>
						<a href="<?php echo base_url('Admin/Staff') ?>" class="btn btn-danger ml-2 mt-3">Back</a>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
