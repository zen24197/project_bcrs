<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid px-4 mt-4 ">
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item active">Court</li>
			</ol>
			<table id="CTable" class="table table-bordered mb-4">
				<thead>
					<tr style="border: none;">
						<th colspan="5">
							<h3 class="text-center">Court List</h3>
						</th>
						<th>
							<center>
								<a href="<?php echo site_url('Admin/Add_Form/') ?>" class="btn btn-primary mb-2">
									<i class="fa-solid fa-plus" style="color: #ffffff;"></i>
								</a>
							</center>
						</th>
					</tr>

					<tr>

						<th class="text-center">No</th>
						<th class="text-center">Court Name</th>
						<th class="text-center">Court Type</th>
						<th class="text-center">Condition</th>
						<th class="text-center">Price</th>
						
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1; ?>
					<?php foreach ($court as $row) : ?>
						<tr>
							<td>
								<?php echo $no; ?>
							</td>
							<td>
								<?php echo $row->name; ?>
							</td>
							<td class="text-center">
								<?php echo $row->type; ?>
							</td>
							<td class="text-center">
								<?php echo $row->condition; ?>
							</td>
							<td class="text-center">
								RM <?php echo $row->price; ?>
							</td>
					
							<td class="text-center">

								<!-- <button class="btn btn-primary mb-3 float-end edit-court" data-url="<?php echo site_url('Admin/modalDetail/') ?>/<?php echo $row->id_court; ?>" data-toggle="modal" data-target="#myModalEdit"><i class="fa-solid fa-pen-to-square" style="color: #ffffff;"></i></button>  -->

								<a href="<?php echo site_url('Admin/Edit_Form/') ?>/<?php echo $row->id_court; ?>" class="btn btn-warning"><i class="fa-solid fa-pen-to-square" style="color: #ffffff;"></i></a>


								<!-- 								<a href="#" class="btn btn-warning edit-court" data-id="<?php echo $row->id_court; ?>"><i class="fa-solid fa-pen-to-square" style="color: #ffffff;"></i></a> -->
								<a href="<?php echo site_url('Admin/DeleteCourt') ?>/<?php echo $row->id_court; ?>" class="btn btn-danger" onclick="return sahkan()"><i class="fa-solid fa-trash" style="color: #ffffff;"></i></a>
							</td>
						</tr>
						<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>
			</table>

			<!-- Add a data attribute to store the court ID in the button -->
			<!-- 			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
             Modal content goes here 
            <div class="modal-body">
                 Display the court_id from the session 
                Court ID: <?php echo $this->session->userdata('court_id'); ?>
            </div>
        </div>
    </div>
</div> -->

			<!-- Modal for Editing Data -->

		</div>
	</main>

	<!--  -->
	<!--  -->

	<footer class="py-4 bg-light mt-auto">
		<div class="container-fluid px-4">
			<div class="d-flex align-items-center justify-content-between small">
				<div class="text-muted">Copyright &copy;</div>
				<div>
					<a href="#">Privacy Policy</a>
					&middot;
					<a href="#">Terms &amp; Conditions</a>
				</div>
			</div>
		</div>
	</footer>

</div>
</div>
<script>
	/* 	function openFormEdit(id_court) {
		document.getElementById("myForm2").style.display = "block";
		// set the value of the hidden input field with the empid value
		document.getElementById("id_court").value = id_court;
	} */

	function sahkan() {
		return confirm('are you sure?');
	}
</script>
