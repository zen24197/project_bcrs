<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://kit.fontawesome.com/c52decb24a.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/styles.css" />

	<!-- Datatables CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/datatables/dataTables.bootstrap5.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/datatables/buttons.bootstrap5.min.css">
	<!-- End Datatables CSS -->

	<!-- Select2 CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/select2/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/select2/select2-bootstrap-5-theme.min.css" />
	<!-- End Select2 CSS -->

	<!-- SweetAlert2 -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/libs/sweetalert2/sweetalert2.min.css" />
	<!-- End SweetAlert2 -->

	<script src="js/jquery-3.6.4.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


	<link rel="stylesheet" href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
	<title>Document</title>
</head>


<body class="sb-nav-fixed">
	<?php
	$user_name = $this->session->userdata('username');
	$role = $this->session->userdata('is_super');
	$role_label = ($role == 1) ? 'Super Admin' : 'Admin';
	?>
	<nav class="sb-topnav navbar navbar-expand bg-dark">
		<!-- Navbar Brand-->
		<a class="navbar-brand ps-3 text-white" href="<?= site_url() ?>Admin">BCRS</a>
		<!-- Sidebar Toggle-->
		<button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars" style="color: white;"></i></button>
		<!-- Navbar Search-->

		<!-- Navbar-->
		<ul class="navbar-nav ms-auto me-0 me-md-3 my-2 my-md-0 me-lg-4">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw" style="color: white;"></i></i></a>
				<ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
	
					<li><a class="dropdown-item" href="<?= site_url() ?>Auth/logout">Logout</a></li>
				</ul>
			</li>
		</ul>
	</nav>
	<div id="layoutSidenav">
		<div id="layoutSidenav_nav">
			<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
				<div class="sb-sidenav-menu">
					<div class="nav">
				
						<div class="sb-sidenav-menu-heading">Management</div>
						<a class="nav-link collapsed" href="<?= site_url() ?>Admin/court">
							<div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
							Court
						</a>
						<a class="nav-link" href="<?= site_url() ?>Admin/balance">
							<div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
							Payment
						</a>
						<?php if ($role == 1) : ?>
							<!-- Display this button only for Super Admin (role 1) -->
							<a class="nav-link" href="<?= site_url() ?>Admin/Staff">
								<div class="sb-nav-link-icon"><i class="fas fa-plus-circle"></i></div>
								Staff
							</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="sb-sidenav-footer">
					Username : <?php echo $user_name ?> <br>
					Role: <?php echo $role_label; ?>
				</div>
			</nav>
		</div>
