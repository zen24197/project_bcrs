<div id="layoutSidenav_content">
	<div class="container mt-5 ms-4">
		<ol class="breadcrumb">
			<li class="breadcrumb-item active">Court</li>
		</ol>
		<div class="row">
			<div class="col-md-6 offset-md-1">
				<input type="hidden" class="form-control" id="stdId" name="idCourt" value="<?Php echo $row_detail->id_booking ?>" required>
				<div class="form-group mt-5">
					<label for="courtName">Customer Name</label>
					<input type="text" class="form-control" id="id_customer" placeholder="Enter Court Name" name="id_customer" readonly value="<?Php echo $row_detail->id_customer ?>">
				</div>
				<div class="form-group mt-5">
					<label for="courtName">Court Name</label>
					<input type="text" class="form-control" id="id_court" placeholder="Enter Court Name" name="id_court" readonly value="<?Php echo $row_detail->id_court ?>">
				</div>
				<div class="form-group">
					<label for="courtPrice">Booking Date </label>
					<input type="date" class="form-control" id="courtPrice" placeholder="Enter Court Price" name="booking_date" readonly value="<?Php echo $row_detail->booking_date ?>">
				</div>
				<div class="form-group">
					<label for="depositRate">Booking Time</label>
					<input type="date" class="form-control" id="depositRate" placeholder="Enter Deposit Rate" name="booking_time" readonly value="<?Php echo $row_detail->booking_time ?>">
				</div>
				<div class="form-group">
					<label for="depositRate">Duration</label>
					<input type="number" class="form-control" id="depositRate" placeholder="Enter Deposit Rate" name="duration" readonly value="<?Php echo $row_detail->duration ?>">
				</div>
				<div class="form-group">
					<label for="depositRate">Total Payment</label>
					<input type="number" class="form-control" id="depositRate" placeholder="Enter Deposit Rate" name="total_payment" readonly step="0.01" value="<?Php echo $row_detail->total_payment ?>">
				</div>
				<div class="form-group">
					<label for="depositRate">Deposit Payment</label>
					<input type="number" class="form-control" id="depositRate" placeholder="Enter Deposit Rate" name="deposit_payment" step="0.01" value="<?Php echo $row_detail->deposit_payment ?>">
				</div>
				<div class="form-group">
					<label for="depositRate">Deposit Status</label>
					<input type="text" class="form-control" id="depositRate" placeholder="Enter Deposit Rate" name="deposit_status" value="<?Php echo $row_detail->deposit_status ?>">
				</div>
				<div>
					<a href="<?php echo base_url('Admin/Balance') ?>" class="btn btn-danger ml-5 mt-3">Back</a>
				</div>
			</div>
		</div>
	</div>
</div>
