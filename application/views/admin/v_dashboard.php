<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid px-4 mt-4 text-center">
			<h2 class="mt-5">WELCOME TO BADMINTON COURT RESERVATION SYSTEM</h2>
		</div>
	</main>

	<footer class="py-4 bg-light mt-auto">
		<div class="container-fluid px-4">
			<div class="d-flex align-items-center justify-content-between small">
				<div class="text-muted">Copyright &copy;</div>
				<div>
					<a href="#">Privacy Policy</a>
					&middot;
					<a href="#">Terms &amp; Conditions</a>
				</div>
			</div>
		</div>
	</footer>
</div>
</div>
