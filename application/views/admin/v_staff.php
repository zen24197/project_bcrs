<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid px-4 mt-4 ">
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item active">Staff</li>
			</ol>
			<table id="CTable" class="table table-bordered mb-4">
				<thead>
					<tr style="border: none;">
						<th colspan="4">
							<h3 class="text-center">Staff List</h3>
						</th>
						<th>
							<center>
								<a href="<?php echo site_url('Admin/Add_StaffForm/') ?>" class="btn btn-primary mb-2">
									<i class="fa-solid fa-plus" style="color: #ffffff;"></i>
								</a>
							</center>
						</th>
					</tr>

					<tr>

						<th class="text-center">No</th>
						<th class="text-center">Name</th>
						<th class="text-center">Username</th>
						<th class="text-center">Permision</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1; ?>
					<?php foreach ($admin as $row) : ?>
						<tr>
							<td>
								<?php echo $no; ?>
							</td>
							<td>
								<?php echo $row->name; ?>
							</td>
							<td class="text-center">
								<?php echo $row->username; ?>
							</td>
							<td class="text-center">
								<?php echo ($row->is_active == 1) ? 'Active' : 'Deactive'; ?>
							</td>


							<td class="text-center">
								<a href="<?php echo site_url('Admin/Edit_Staff') ?>/<?php echo $row->id_admin; ?>" class="btn btn-warning"><i class="fa-solid fa-pen-to-square" style="color: #ffffff;"></i></a>
							</td>
						</tr>
						<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
</div>
</div>
</div>



</div>
</main>

<!--  -->
<!--  -->

<footer class="py-4 bg-light mt-auto">
	<div class="container-fluid px-4">
		<div class="d-flex align-items-center justify-content-between small">
			<div class="text-muted">Copyright &copy;</div>
			<div>
				<a href="#">Privacy Policy</a>
				&middot;
				<a href="#">Terms &amp; Conditions</a>
			</div>
		</div>
	</div>
</footer>

</div>
</div>
<script>
	/* 	function openFormEdit(id_court) {
		document.getElementById("myForm2").style.display = "block";
		// set the value of the hidden input field with the empid value
		document.getElementById("id_court").value = id_court;
	} */

	function sahkan() {
		return confirm('are you sure?');
	}
</script>
