<div id="layoutSidenav_content">
	<main>
		<div class="container-fluid px-4 mt-4 ">
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item active">Booking</li>
			</ol>
			<table id="CTable" class="table table-bordered mb-4">
				<thead>
					<tr style="border: none;">
						<th colspan="6">
							<h3 class="text-center">Booking Payment</h3>
						</th>

					</tr>

					<tr>

						<th class="text-center">No</th>
						<th class="text-center">Customer Name</th>
						<th class="text-center">Court Name</th>
						<th class="text-center">Total Payment</th>
						<th class="text-center">Status</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1; ?>
					<?php foreach ($booking as $row) : ?>
						<tr>
							<td>
								<?php echo $no; ?>
							</td>
							<td class="text-center">
								<?php echo $row->name; ?>
							</td>
							<td class="text-center">
								<?php echo $row->court_name; ?>
							</td>
							<td class="text-center">
								<?php echo $row->total_payment; ?>
							</td>
							<td class="text-center">
								<?php
								$status = $row->status;
								$status_text = '';

								switch ($status) {
									case 0:
										$status_text = 'Pending';
										break;
									case 1:
										$status_text = 'Refund';
										break;
									case 2:
										$status_text = 'Complete';
										break;
									default:
										$status_text = 'Unknown';
										break;
								}

								echo $status_text;
								?>
							</td>



							<td class="text-center">

								<!-- <button class="btn btn-primary mb-3 float-end edit-court" value="<?php echo $row->id_court; ?> " data-toggle="modal" data-target="#myModalEdit"><i class="fa-solid fa-pen-to-square" style="color: #ffffff;"></i></button> -->
								<a href="<?php echo site_url('Admin/editstatus_Form/') ?>/<?php echo $row->id_booking; ?>" class="btn btn-warning"><i class="fa-solid fa-pen-to-square" style="color: #ffffff;"></i></a>

						</tr>
						<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
			=

			<!-- Modal for Editing Data -->

		</div>
	</main>

	<!--  -->
	<!--  -->

	<footer class="py-4 bg-light mt-auto">
		<div class="container-fluid px-4">
			<div class="d-flex align-items-center justify-content-between small">
				<div class="text-muted">Copyright &copy;</div>
				<div>
					<a href="#">Privacy Policy</a>
					&middot;
					<a href="#">Terms &amp; Conditions</a>
				</div>
			</div>
		</div>
	</footer>

</div>
</div>
<script>
	function openFormEdit(id_court) {
		document.getElementById("myForm2").style.display = "block";
		// set the value of the hidden input field with the empid value
		document.getElementById("id_court").value = id_court;
	}

	function sahkan() {
		return confirm('are you sure?');
	}
</script>
