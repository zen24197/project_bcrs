
<?php

class m_authModel extends CI_Model
{


	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function register_user()
	{

		$password = $this->input->post('password');
		$con_password = $this->input->post('con_password');


		if ($password != $con_password) {
			$this->session->set_flashdata('worng', 'The password not equal with confirmation!');
			redirect('Auth/register');
		} else {
			$data = array(
				"name" => $this->input->post('name'),
				"email" => $this->input->post('email'),
				"password" => $password
			);

			$this->db->insert('users', $data);
			$this->session->set_flashdata('suc', 'You are registered please login');
			redirect('Auth/');
		}
	}
	public function register_admin()
	{

		$password = $this->input->post('password');
		$con_password = $this->input->post('con_password');


		if ($password != $con_password) {
			$this->session->set_flashdata('worng', 'The password not equal with confirmation!');
			redirect('Auth/register');
		} else {
			$data = array(
				"name" => $this->input->post('name'),
				"email" => $this->input->post('email'),
				"password" => $password
			);

			$this->db->insert('users', $data);
			$this->session->set_flashdata('suc', 'You are registered please login');
			redirect('Auth/');
		}
	}

	public function login_user()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$query = $this->db->get('users');
		$find_user = $query->num_rows($query);

		if ($find_user > 0) {
			$this->session->set_flashdata('suc', 'You are logged');
			redirect('Auth/main');
		} else {
			$this->session->set_flashdata('warning', 'Incorrect Authentication!!!');

			redirect('Auth/');
		}
	}
	public function getCourtPriceById($court_id, $duration)
	{
		$this->db->select('price');
		$this->db->from('court');
		$this->db->where('id_court', $court_id);

		$query = $this->db->get();

		// Check if a row was found
		if ($query->num_rows() > 0) {
			$row = $query->row();
			$price = $row->price;
			$total_payment = $price * $duration;
			return ['price' => $price, 'total_price' => $total_payment];
		} else {
			return ['price' => 0, 'total_price' => 0];
		}
	}
	public function finishBook($data_book)
	{
		$booking_date = $data_book['booking_date'];
		$booking_start_time = $data_book['booking_start_time'];
		$booking_end_time = $data_book['booking_end_time'];
	
		$overlapQuery = "
			SELECT *
			FROM booking
			WHERE booking_date = '$booking_date'
			  AND (
					(booking_start_time <= '$booking_end_time' AND booking_end_time >= '$booking_start_time') 
					OR 
					(booking_start_time >= '$booking_start_time' AND booking_end_time <= '$booking_end_time')
			  )
		";
	
		$overlapResult = $this->db->query($overlapQuery);
	
		return $overlapResult->num_rows() === 0 ? true : false;
	}
	


	public function getCourtBookingData()
	{
		$this->db->select("id_court,name,type,condition,price");
		$this->db->from("court");
		/* 		$this->db->join('sub_continent sc', 'c.sub_continent_id = sc.sub_continent_id', 'right');
		$this->db->where('c.idcountry',$id);
		$this->db->group_by('c.idcountry'); */
		$query = $this->db->get();
		return $query->result();
	}
	public function  calculateBook()
	{
	}
	public function findCourtData($data)
	{
		$this->db->select('check_date');
		$this->db->from('checkup');
		$this->db->where('check_date', $data);

		$query = $this->db->get();

		$price = $query->row();


		return $price;
	}
	public function saveDateTime($selected_data)
	{
		foreach ($selected_data as $data) {
			// Replace 'your_table_name' with the actual table name where you want to save the data
			$this->db->insert('checkup', $data);
		}
	}
	public function getCourt()
	{

		/* 		$this->db->limit($limit);
		$this->db->offset($offset); */

		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('court');
		return $query->result();
	}
	public function getBooking()
	{
		$this->db->select('booking.*, customer.name, court.name as court_name');
		$this->db->from('booking');
		$this->db->join('customer', 'customer.id_customer = booking.id_customer', 'left');
		$this->db->join('court', 'court.id_court = booking.id_court', 'left'); // Assuming id_court is the foreign key in the booking table and court table

		$query = $this->db->get();
		return $query->result();
	}
	public function getStaff()
	{
		$this->db->select('*');
		$this->db->from('Admin');
		$this->db->where('is_super', 0);
		$query = $this->db->get();
		return $query->result();
	}

	function insertCourt($data)
	{
		$this->db->insert('court', $data);
	}
	function deleteCourt($data)
	{
		$this->db->where('id_court', $data);
		$this->db->delete('court');
	}
	function getEditData($data)
	{
		$this->db->where('id_court', $data);
		$query = $this->db->get('court');
		return $query->row();
	}
	function getEditStatusData($data)
	{
		$this->db->where('id_booking', $data);
		$query = $this->db->get('booking');
		return $query->row();
	}
	function getDetailStatusData($data)
	{
		$this->db->where('id_booking', $data);
		$query = $this->db->get('booking');
		return $query->row();
	}

	function insertStaff($data)
	{
		$this->db->insert('admin', $data);
	}
	function updateCourt($id, $data)
	{
		$this->db->where('id_court', $id);
		$this->db->update('court', $data);
	}
	function updateStaff($id, $data)
	{
		$this->db->where('id_admin', $id);

		$this->db->update('admin', $data);
	}
	/* 	$this->db->select("id_booking,id_customer,id_court,booking_date,booking_time,duration,total_payment");
    $this->db->from("country c");
    $this->db->join('sub_continent sc', 'c.sub_continent_id = sc.sub_continent_id', 'right');
    $this->db->where('c.idcountry',$id);
    $this->db->group_by('c.idcountry'); */

	/*  $query = $this->db->get();//template */
	/*    if ($query->num_rows() > 0) { */
	/* return $query->row();//$query->result() beban sistem= keluar semua//$query->row() =keluar satu */
	function updateStatus($id, $data)
	{
		$this->db->select("c.country_name,sc.name,c.status_application, 
		c.country_status, sc.code_matrix,c.sub_continent_id,c.idcountry");
		$this->db->where('id_booking', $id);
		$this->db->update('booking', $data);
	}

	function getEditStaffData($data)
	{
		$this->db->where('id_admin', $data);
		$query = $this->db->get('admin');
		return $query->row();
	}
	function getDetail($id)
	{
		$this->db->where('id_court', $id);
		$query = $this->db->get('court');
		return $query->row();
	}


	/* 	function insertStudent($data)
	{
		$this->db->insert('student', $data);
	}
	function deleteStudent($data)
	{
		$this->db->where('student_id', $data);
		$this->db->delete('student');
	}
	function getEditData($data)
	{
		$this->db->where('student_id', $data);
		$query = $this->db->get('student');
		return $query->row();
	}
	function updateStudent($id, $data)
	{
		$this->db->where('student_id', $id);
		$this->db->update('student', $data);
	} */
}

?>
