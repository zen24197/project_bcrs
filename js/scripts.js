/*!
	* Start Bootstrap - SB Admin v7.0.7 (https://startbootstrap.com/template/sb-admin)
	* Copyright 2013-2023 Start Bootstrap
	* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-sb-admin/blob/master/LICENSE)
	*/
// 
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {

	// Toggle the side navigation
	const sidebarToggle = document.body.querySelector('#sidebarToggle');
	if (sidebarToggle) {
		// Uncomment Below to persist sidebar toggle between refreshes
		// if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
		//     document.body.classList.toggle('sb-sidenav-toggled');
		// }
		sidebarToggle.addEventListener('click', event => {
			event.preventDefault();
			document.body.classList.toggle('sb-sidenav-toggled');
			localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
		});
	}

});

/* custom */
$(document).ready(function () {
	// Handle form submission
	$("#myModal").on("click", ".btn-primary", function () {
		// Get form data
		var courtName = $("#courtName").val();
		// Add additional form data as needed

		// Send data to the server via AJAX or perform other actions

		// Close the modal
		$("#myModal").modal("hide");
	});
});
/* $(document).ready(function () {
	// Handle form submission
	$("#myModalEdit").on("click", ".btn-primary", function () {
		// Get form data
		var courtName = $("#courtName").val();
		// Add additional form data as needed

		// Send data to the server via AJAX or perform other actions

		// Close the modal
		$("#myModalEdit").modal("hide");
	});
}); */

$(document).ready(function() {
    $(".edit-court").click(function() {
        // Get the court ID from the data-id attribute
        var courtId = $(this).data("id");

        // Set the court ID in the modal's hidden input field
        $("#editCourtId").val(courtId);

        // Show the modal
        $("#myModalEdit").modal("show");
    });
});

function openFormEdit(empid, id, nama, phonenum) {
	// Populate modal fields with data
	document.getElementById('empid').value = empid;
	document.getElementById('id').value = id;
	document.getElementById('nama').value = nama;
	document.getElementById('phonenum').value = phonenum;
	
	// Show the modal
	$('#editModal').modal('show');
  }
  
  function saveChanges() {
	// Get values from modal fields
	var empid = document.getElementById('empid').value;
	var id = document.getElementById('id').value;
	var nama = document.getElementById('nama').value;
	var phonenum = document.getElementById('phonenum').value;
  
	// Perform your save logic here
  
	// Close the modal
	$('#editModal').modal('hide');
  }
  
/*   function sendCourtIdToSession(courtId) {
    // Use AJAX to send the court_id to a CodeIgniter controller method
    $.ajax({
        url: "<?php echo site_url('your_controller/set_court_id_session'); ?>",
        type: "POST",
        data: { court_id: courtId },
        success: function(response) {
            // Handle success (if needed)
            // You can display a modal here after successful completion
            // Example: $('#myModal').modal('show');
			$('#editModal').modal('show');
        },
        error: function() {
            // Handle errors (if needed)
        }
    });
} */
  
/*  */
